require("dotenv").config();

const {
  lerInput,
  inquirerMenu,
  pausa,
  listarLugares,
} = require("./helpers/inquirer");
const Buscas = require("./models/buscas");

const main = async () => {
  const buscas = new Buscas();
  let opt;

  do {
    opt = await inquirerMenu();

    switch (opt) {
      case 1:
        // mostrar mensagem
        const termo = await lerInput("Cidade: ");

        // Buscar os lugares
        const lugares = await buscas.cidade(termo);
        // selecionar o lugar
        const id = await listarLugares(lugares);
        if (id === "0") continue;
        const lugarSelecionado = lugares.find((lugarX) => lugarX.id === id);
        const { nome, lat, lng } = lugarSelecionado;
        // clima
        // mostrar resultados
        // Mostrar resultados
        // console.clear();
        console.log("\nInformações da cidade\n".green);
        console.log("Cidade:", nome);
        console.log("Lat:", lat);
        console.log("Lng:", lng);
        console.log("Temperatura:");
        console.log("Mínima:");
        console.log("Máxima:");
        console.log("Como está o clima:");

      default:
        break;
    }

    if (opt !== 0) await pausa();
  } while (opt !== 0);
};

main();
