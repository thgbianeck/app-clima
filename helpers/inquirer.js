const inquirer = require("inquirer");
require("colors");

const perguntas = [
  {
    type: "list",
    name: "opcao",
    message: "O que desejas fazer?",
    choices: [
      {
        value: 1,
        name: `${"1.".green} Procurar cidade`,
      },
      {
        value: 2,
        name: `${"2.".green} Historico`,
      },
      {
        value: 0,
        name: `${"0.".green} Sair`,
      },
    ],
  },
];

const inquirerMenu = async () => {
  console.clear();
  console.log("==========================".green);
  console.log("  Selecione uma opcao".white);
  console.log("==========================\n".green);

  const { opcao } = await inquirer.prompt(perguntas);

  return opcao;
};

const pausa = async () => {
  const question = [
    {
      type: "input",
      name: "enter",
      message: `Pressione ${"ENTER".green} para continuar`,
    },
  ];

  console.log("\n");
  await inquirer.prompt(question);
};

const lerInput = async (message) => {
  const question = [
    {
      type: "input",
      name: "desc",
      message,
      validate(value) {
        if (value.length === 0) {
          return "Por favor insira um valor";
        }
        return true;
      },
    },
  ];

  const { desc } = await inquirer.prompt(question);
  return desc;
};

const listarLugares = async (lugares = []) => {
  const choices = lugares.map((lugar, i) => {
    const idx = `${i + 1}.`.green;

    return {
      value: lugar.id,
      name: `${idx} ${lugar.nome}`,
    };
  });

  choices.unshift({
    value: "0",
    name: "0.".green + " Cancelar",
  });

  const perguntas = [
    {
      type: "list",
      name: "id",
      message: "Selecione o lugar:",
      choices,
    },
  ];

  const { id } = await inquirer.prompt(perguntas);
  return id;
};

const confirmar = async (message) => {
  const question = [
    {
      type: "confirm",
      name: "ok",
      message,
    },
  ];

  const { ok } = await inquirer.prompt(question);
  return ok;
};

const mostrarListaChecklist = async (tarefas = []) => {
  const choices = tarefas.map((tarea, i) => {
    const idx = `${i + 1}.`.green;

    return {
      value: tarefa.id,
      name: `${idx} ${tarefa.desc}`,
      checked: tarefa.completadoEm ? true : false,
    };
  });

  const pregunta = [
    {
      type: "checkbox",
      name: "ids",
      message: "Selecoes",
      choices,
    },
  ];

  const { ids } = await inquirer.prompt(pergunta);
  return ids;
};

module.exports = {
  inquirerMenu,
  pausa,
  lerInput,
  listarLugares,
  confirmar,
  mostrarListaChecklist,
};
