const fs = require("fs");
const axios = require("axios");

class Buscas {
  historico = ["Brasília", "São Paulo", "Curitiba", "Manaus"];

  constructor() {
    // TODO: ler db se ele existe
  }

  get paramsMapbox() {
    return {
      access_token: process.env.MAPBOX_KEY,
      limit: 5,
      language: "pt-br",
    };
  }

  async cidade(lugar = "") {
    try {
      const instance = axios.create({
        baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${lugar}.json`,
        params: this.paramsMapbox,
      });

      const resp = await instance.get();

      return resp.data.features.map((lugar) => ({
        id: lugar.id,
        nome: lugar.place_name,
        lng: lugar.center[0],
        lat: lugar.center[1],
      }));
    } catch (error) {
      return [];
    }
  }
}

module.exports = Buscas;
