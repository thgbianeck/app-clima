# App Clima

## App de clima desenvolvido com NodeJS que se concentra nos seguintes tópicos:

- Consumo de API
- Chamadas HTTP para servidores externos
- Solicitar pacote - superficialmente
- Pacote Axios
- Mapbox lugares para obter lugares por nome
- Usando OpenWeather para obter o clima
- Aplicativo de console com histórico
- Variáveis de ambiente
